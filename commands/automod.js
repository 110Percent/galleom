const Discord = require('discord.js');
const db = require('../core/database');

module.exports = {
  usage: '<add/list> <word>',
  description: 'Adds/lists automod flagged words.',
  example: '=automod add sweaty',
  aliases: ['am'],
  hidden: true,
  modOnly: true
};

module.exports.action = (_client, msg, args) => {
  if (!args) return;

  if (args.split(' ')[0] === 'add') {
    // Create db entry for new flag
    db.bannedWords
      .create({
        name: args.split(' ')[1],
        regex: args.split(' ')[2] || args.split(' ')[1]
      })
      .then((newWord) => {
        msg.channel.send(`Added automod flag for ${newWord.dataValues.name}`);
      });
  } else if (args.split(' ')[0] === 'list') {
    // Get flags from db and display them in an embed messaged to the sender
    db.bannedWords.findAll().then((words) => {
      const embed = new Discord.RichEmbed();
      embed.setTitle('AutoMod Banned Words');
      const wordsString = words
        .map(w => `${w.dataValues.id}. **${w.dataValues.name}** (\`${w.dataValues.regex}\`)`)
        .join('\n');
      embed.setDescription(wordsString);
      msg.author.send(embed).then(() => {
        // React to confirm delivery
        msg.react('✅');
      });
    });
  } else if (args.split(' ')[0] === 'delete') {
    // Only accept numbered indexes
    if (Number.isNaN(args.split(' ')[1])) {
      msg.channel.send(
        'Index to remove not provided. Please include the number of the automod query to delete.'
      );
      return;
    }
    // Delete row from db
    db.bannedWords.sync().then(() => {
      db.bannedWords
        .destroy({
          where: {
            id: Number(args.split(' ')[1])
          }
        })
        .then(() => {
          msg.channel.send('Instance deleted.');
        });
    });
  }
};
