const config = require('../config/config');

module.exports = {
  usage: '<user ID>',
  description:
    'Brings up a prompt to ban a user from the main server using an ID. User does not have to be in the server to be banned.',
  example: `=ban ${config.ownerID}`,
  aliases: ['hammer'],
  hidden: true,
  modOnly: true
};

module.exports.action = (client, msg, args) => {
  const [query] = args.split(' ');

  // Only accept numbered IDs
  // TODO: edit to accept pings
  if (!new RegExp('^\\d+$').test(query)) {
    msg.channel.send('User ID not provided.');
    return;
  }

  // Confirmation message displays an ID if the user is not "real", or in the main server on ban
  let user = client.users.get(query);
  let real = false;
  if (user) {
    real = true;
    user = user.tag;
  } else {
    user = query;
  }

  msg.channel
    .send(
      `Are you sure you want to ban User \`${user}\`? ${
        !real ? '\n⚠ This user is not currently in the server!' : ''
      }`
    )
    .then((msg2) => {
      msg2.react('👌');
      // For confirmation purposes, only ban after the reaction is clicked
      msg2
        .awaitReactions((r, u) => r.emoji.name === '👌' && u.id === msg.author.id, { max: 1 })
        .then((collected) => {
          if (collected.size > 0) {
            msg2.delete();
            client.guilds
              .get(config.mainServer)
              .ban(query)
              .then(() => {
                msg.channel.send(`Banned ${user} from the server.`);
              })
              .catch(err => msg.channel.send(`Error banning user: ${err.message}`));
          }
        })
        .catch(console.error);
    });
};
