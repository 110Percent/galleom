const config = require('../config/config');
const db = require('../core/database');
const msgLogger = require('../core/msgLogger');

module.exports = {
  usage: '<user ID>',
  description:
    'Pulls a user into an isolated channel to talk to a moderator. Automatically deleted and logs the channel afterwards.',
  example: `=detention ${config.ownerID}`,
  aliases: ['det'],
  hidden: true,
  modOnly: true
};

async function detention(user, msg, client) {
  const mainServer = client.guilds.get(config.mainServer);
  // Create sanitized version of username for use with channel creation
  const suffix = user.user.username.replace(/[^a-zA-Z0-9\-_]/g, '').toLowerCase();
  const c = await mainServer.createChannel(`detention-${suffix}`, 'text', [
    {
      denied: ['VIEW_CHANNEL'],
      id: mainServer.id
    },
    {
      allowed: ['VIEW_CHANNEL'],
      id: client.user.id
    },
    {
      allowed: ['VIEW_CHANNEL'],
      id: config.modRole
    },
    {
      allowed: ['VIEW_CHANNEL'],
      id: config.trialModRole
    }
  ]);
  // Move channel to the detention category
  await c.setParent(config.detCategory);
  // Allow the detentioned user to see the channel
  await c.overwritePermissions(user.user, { VIEW_CHANNEL: true });
  // Overwrite/create the cached roles of the muted user
  await db.muteCache.findOrCreate({ where: { user: user.user.id } });
  const cachedUser = await db.muteCache.findOne({ where: { user: user.user.id } });
  const cachedRoles = await db.cachedRoles.findAll();
  cachedUser.roles = user.roles
    .map(u => cachedRoles.find(r => r.dataValues.roleID === u.id).dataValues.id)
    .join(';');
  cachedUser.channel = c.id;
  await cachedUser.save();
  // Wipe user's roles and add detention role
  try {
    await user.removeRoles(user.roles.map(r => r.id));
  } catch (err) {
    console.error(err);
  }
  await user.addRole(config.detRole);
  // Begin logging detention channel
  msgLogger.startLogging(c.id);
  c.send(`${user}\n\nYou have been detentioned. Please wait here for a mod to contact you.`);
  msg.channel.send(`User was detentioned to ${c}.`);
  const cUser = await db.cachedUsers.findOne({ where: { userID: user.user.id } });
  const cacheUpdate = {
    userID: user.user.id,
    username: user.user.username,
    discriminator: user.user.discriminator.toString(),
    displayAvatarURL: user.user.displayAvatarURL
  };
  if (!cUser) {
    db.cachedUsers.create(cacheUpdate);
  } else {
    db.cachedUsers.update(cacheUpdate, { where: { userID: cacheUpdate.userID } });
  }
}

async function undetention(user, client, msg) {
  const mainServer = client.guilds.get(config.mainServer);
  const member = mainServer.members.get(user);
  // Fetch detention data from db
  const mute = await db.muteCache.findOne({ where: { user } });
  const { channel } = mute.dataValues;
  // Stop logging detention channel
  msgLogger.stopLogging(channel);
  // Write channel logs to db
  await db.channelLogs.create({
    channel,
    channelName: client.channels.get(channel).name,
    logs: JSON.stringify(msgLogger.logs[channel])
  });
  // Remove user from detention and grant revoked roles back
  if (member) {
    try {
      await member.removeRole(config.detRole);
    } catch (err) {
      console.error(err);
    }
    const cached = await db.cachedRoles.findAll();
    const roles = mute.dataValues.roles.split(';');
    for (let i = 0; i < roles.length; i++) {
      const thisRole = cached.find(c => c.dataValues.id.toString() === roles[i]);
      if (thisRole) {
        if (thisRole.roleID === member.guild.id) continue;
        await member.addRole(thisRole.roleID);
      }
    }
    db.muteCache.destroy({ where: { user } });
  }
  // Delete detention channel and provide logs link
  mainServer.channels
    .get(channel)
    .delete()
    .then(() => {
      const logURL = `https://galleom.110percent.xyz/logs/${channel}`;
      msg.channel.send(
        `User removed from detention. Click here to view a log of the conversation:\n${logURL}`
      );
    });
}

module.exports.action = (client, msg, args) => {
  // Accept user IDs
  const query = args.split(' ')[0];
  if (!new RegExp('^\\d+$').test(query)) {
    msg.channel.send('User ID not provided.');
    return;
  }

  const user = client.guilds.get(config.mainServer).members.get(query);
  if (!user) {
    msg.channel.send('❌ User not found.');
    return;
  }
  msg.channel
    .send(`Are you sure you want to detention User \`${user.user.username}\`?`)
    .then((msg2) => {
      // Await reaction for confirmation
      msg2.react('👌');
      msg2
        .awaitReactions((r, u) => r.emoji.name === '👌' && u.id === msg.author.id, { max: 1 })
        .then((collected) => {
          if (collected.size > 0) {
            detention(user, msg, client);
          }
        })
        .catch(console.error);
    });
};

module.exports.undetention = undetention;
