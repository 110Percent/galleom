module.exports = {
  usage: '<code>',
  description: "Evaluate JavaScript code. For use by the bot's owner only.",
  example: '=eval "hi"',
  hidden: true,
  ownerOnly: true
};

module.exports.action = (_client, msg, args) => {
  let toPrint;
  try {
    // eslint-disable-next-line no-eval
    toPrint = eval(args);
  } catch (err) {
    msg.channel.send(err.toString());
    return;
  }
  msg.channel.send(toPrint);
};
