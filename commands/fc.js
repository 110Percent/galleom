const Discord = require('discord.js');
const Sequelize = require('sequelize');
const db = require('../core/database');
const config = require('../config/config');

module.exports = {
  usage: '<set/get/consoles> [user/console] [code]',
  description:
    "Gets a user's previously-set friend code, or sets your own for others to view. Use the `consoles` subcommand to view a list of compatible consoles.",
  example: `=fc get <@${config.ownerID}>\n=fc set switch SW-1234-5678-9012\n=fc consoles`
};

module.exports.action = (client, msg, args) => {
  const sCommand = args.split(' ')[0];
  if (sCommand === 'set') {
    db.consoles
      .findAll({
        where: {
          name: {
            [Sequelize.Op.iLike]: args.split(' ')[1]
          }
        }
      })
      .then((sConsoles) => {
        const tConsole = sConsoles[0].dataValues;
        if (!tConsole) {
          msg.channel.send('Console not found.');
          return;
        }
        if (!new RegExp(tConsole.regex).test(args.split(' ')[2])) {
          msg.channel.send(
            `Invalid friend code format. Please enter your code using the following example: \`${
              tConsole.regPlaceholder
            }\``
          );
          return;
        }
        db.friendCodes
          .create({
            user: msg.author.id,
            code: args.split(' ')[2],
            console: tConsole.index
          })
          .then(() => {
            msg.channel.send(`Set friend code for user ${msg.author.tag}.`);
          });
      });
  } else if (sCommand === 'get') {
    let { id } = msg.author;
    if (
      args.split(' ').length > 1
      && msg.guild.members.get(args.split(' ')[1].replace(/\D/g, ''))
    ) {
      id = args.split(' ')[1].replace(/\D/g, '');
    }
    const user = client.users.get(id);
    db.friendCodes
      .findAll({
        where: {
          user: user.id
        }
      })
      .then((codes) => {
        db.consoles.findAll().then((sConsoles) => {
          const embed = new Discord.RichEmbed();

          embed.setAuthor(`${user.tag}'s friend codes`, user.displayAvatarURL);

          for (let i = 0; i < codes.length; i++) {
            const code = codes[i];
            const tConsole = sConsoles.find(c => c.dataValues.index === code.console);
            embed.addField(tConsole.name, code.code);
          }

          msg.channel.send(embed);
        });
      });
  } else if (sCommand === 'consoles') {
    db.consoles.findAll().then((sConsoles) => {
      const listConsoles = sConsoles.map(c => `\`${c.dataValues.name}\``).join(', ');
      msg.channel.send(`Addable consoles: ${listConsoles}`);
    });
  } else if (sCommand === 'addconsole') {
    if (msg.author.id !== config.ownerID) return;
    db.consoles.max('index').then((indexMax) => {
      const newConsole = {
        name: args.split(' ')[1],
        regex: args.split(' ')[2],
        regPlaceholder: args.split(' ')[3],
        index: Number.isNaN(indexMax) ? 0 : indexMax + 1
      };

      db.consoles.create(newConsole).then(() => {
        msg.channel.send(`Added new console ${newConsole.name}.`);
      });
    });
  }
};
