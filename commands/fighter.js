const {
  createCanvas, loadImage, Image, registerFont
} = require('canvas');
const fs = require('fs');
const path = require('path');

const config = require('../config/config');

module.exports = {
  usage: '[user]',
  description: 'Create a fighter portrait of a user.',
  example: `=fighter\n=fighter <@${config.ownerID}>`
};

registerFont('font/AsimovProUlt.otf', { family: 'Asimov Pro' });

// Thank you, crazy2be from StackOverflow
function getLines(ctx, text, maxWidth) {
  const words = text.split(' ');
  const lines = [];
  let currentLine = words[0];

  for (let i = 1; i < words.length; i++) {
    const word = words[i];
    const { width } = ctx.measureText(`${currentLine} ${word}`);
    if (width < maxWidth) {
      currentLine += ` ${word}`;
    } else {
      lines.push(currentLine);
      currentLine = word;
    }
  }
  lines.push(currentLine);
  return lines;
}

module.exports.action = (client, msg, args) => {
  let query = args.split(' ')[0];
  query = query.match(/\d+/) ? query.match(/\d+/)[0] : msg.author.id;

  let user = client.guilds.get(config.mainServer).members.get(query);
  if (!user) {
    user = msg.guild.members.get(query) ? msg.guild.members.get(query) : client.users.get(query);
  }

  if (!user || user === undefined) {
    msg.channel.send('User not found. Please use a valid mention or ID.');
    return;
  }

  const canvas = createCanvas(906, 388);
  const ctx = canvas.getContext('2d');
  loadImage('img/fighter-bg.png').then((bg) => {
    ctx.drawImage(bg, 0, 0, 906, 388);
    const avatar = new Image();
    avatar.src = user.user.displayAvatarURL || user.displayAvatarURL;
    avatar.onload = () => {
      ctx.drawImage(avatar, 80, -20, 420, 420);
      loadImage('img/fighter-frame.png')
        .then((image) => {
          const toPrint = (user.displayName || user.username).replace(/ /g, '  ');
          ctx.drawImage(image, 0, 0, 906, 388);
          ctx.font = '60px "Asimov Pro" bold';
          ctx.strokeStyle = 'black';
          ctx.textBaseline = 'bottom';
          ctx.textAlign = 'right';
          ctx.lineWidth = 2;
          const lines = getLines(ctx, toPrint, 405);
          for (let i = 0; i < lines.length; i++) {
            const line = lines[i];
            ctx.fillStyle = 'black';
            ctx.fillText(line, 853, 220 - (lines.length - 1 - i) * 70 + 3);
            ctx.fillStyle = 'white';
            ctx.fillText(line, 850, 220 - (lines.length - 1 - i) * 70);
            ctx.strokeText(line, 850, 220 - (lines.length - 1 - i) * 70);
          }
        })
        .then(() => {
          const out = fs.createWriteStream(path.join(`${__dirname}/../img/fighter-temp.png`));
          const stream = canvas.createPNGStream();
          stream.pipe(out);
          out.on('finish', () => {
            msg.channel.send({
              file: 'img/fighter-temp.png',
              name: 'fighter.png'
            });
          });
        });
    };
  });
};
