const Discord = require('discord.js');

module.exports = {
  usage: '[command]',
  description: 'Returns a list of commands, or information on a command if specified.',
  example: '=help\n=help ping',
  aliases: ['h', '?']
};

module.exports.action = (_client, msg, args, commands, aliases) => {
  const embed = new Discord.RichEmbed();
  if (!args) {
    const cmds = [];
    for (let i = 0; i < Object.keys(commands).length; i++) {
      if (!commands[Object.keys(commands)[[i]]].hidden) {
        cmds.push(Object.keys(commands)[i]);
      }
    }
    embed.setTitle('List of available commands');
    embed.setDescription(
      `${cmds
        .map(c => `**=${c}**`)
        .join('\n')}\n\nUse **=help <command>** for additional info on a command.`
    );
    msg.author.send(embed).then(() => {
      msg.react('✅');
    });
  } else {
    let cmd = commands[args.split(' ')[0]];
    if (aliases._all.indexOf(args.split(' ')[0]) > -1) {
      cmd = commands[
        Object.keys(aliases).find(
          key => aliases[key].indexOf(args.split(' ')[0]) > -1 && key !== '_all'
        )
      ];
    }
    embed.setTitle(`=${cmd.name}`);
    embed.setDescription(`Usage: \`=${cmd.name} ${cmd.usage || ''}\`\n\n${cmd.description || ''}`);
    if (cmd.example) {
      embed.addField('Example', cmd.example);
    }
    if (cmd.aliases) {
      embed.addField('Aliases', cmd.aliases.map(a => `=${a}`).join('\n'));
    }
    msg.channel.send(embed);
  }
};
