const Discord = require('discord.js');
const db = require('../core/database');
const config = require('../config/config');

module.exports = {
  usage: '<add/get> <ID> <text>',
  description:
    "Gets a list of a user's modnotes or adds one to said user. Reminder that modnotes are for internal use only, and adding one will not send a message to the subject user.",
  example: `=modnote get ${config.ownerID}\n=modnote add ${
    config.ownerID
  } Spammed in #general and argued with a moderator about it. Mute on next offense.`,
  aliases: ['m'],
  hidden: true,
  modOnly: true
};

module.exports.action = async (client, msg, args) => {
  if (!args) return;

  const ac = args.split(' ')[0];

  if (ac === 'add') {
    const id = args.split(' ')[1];
    const user = client.users.get(id);
    let cUser = await db.cachedUsers.findOne({ where: { userID: id } });
    if (cUser) {
      cUser = cUser.dataValues;
      cUser.seqID = cUser.id;
      cUser.id = cUser.userID;
    }

    if (!user && !cUser) {
      msg.channel.send('Error fetching user.');
      return;
    }
    const content = args.substring(id.length + 5);
    let index;
    db.modnotes
      .findAll({
        where: {
          user: id
        }
      })
      .then((notes) => {
        index = notes.length ? Math.max.apply(null, notes.map(c => c.dataValues.index)) + 1 : 0;
      })
      .then(() => {
        db.modnotes
          .create({
            user: id,
            index,
            content,
            author: msg.author.id
          })
          .then(() => {
            const cacheUpdate = {
              userID: user.id,
              username: user.username,
              discriminator: user.discriminator.toString(),
              displayAvatarURL: user.displayAvatarURL
            };
            if (!cUser) {
              db.cachedUsers.create(cacheUpdate);
            } else {
              db.cachedUsers.update(cacheUpdate, { where: { userID: cacheUpdate.userID } });
            }
            msg.channel.send(
              `Added note for user ${(user || cUser).username}#${(user || cUser).discriminator}.`
            );
          });
      });
  } else if (ac === 'get') {
    const id = args.split(' ')[1];
    const user = client.users.get(id);
    let cUser = await db.cachedUsers.findOne({ where: { userID: id } });
    if (cUser) {
      cUser = cUser.dataValues;
      cUser.seqID = cUser.id;
      cUser.id = cUser.userID;
    }
    if (!user && !cUser) {
      msg.channel.send('Error fetching user.');
    } else {
      db.modnotes
        .findAll({
          where: {
            user: id
          }
        })
        .then((notes) => {
          notes.sort((a, b) => a.dataValues.index - b.dataValues.index);
          const embed = new Discord.RichEmbed();

          embed.setAuthor(
            `Modnotes for ${(user || cUser).username}#${(user || cUser).discriminator} (${
              (user || cUser).id
            })`,
            (user || cUser).displayAvatarURL
          );

          for (let i = 0; i < notes.length; i++) {
            const note = notes[i];
            const author = client.users.get(note.dataValues.author);
            embed.addField(
              `${note.dataValues.index + 1} - ${author.tag} at ${new Date(
                note.dataValues.createdAt
              ).toUTCString()}`,
              note.dataValues.content
            );
          }

          msg.channel.send(embed);
          const cacheUpdate = {
            userID: user.id,
            username: user.username,
            discriminator: user.discriminator.toString(),
            displayAvatarURL: user.displayAvatarURL
          };
          if (!cUser) {
            db.cachedUsers.create(cacheUpdate);
          } else {
            db.cachedUsers.update(cacheUpdate, { where: { userID: cacheUpdate.userID } });
          }
        });
    }
  }
};
