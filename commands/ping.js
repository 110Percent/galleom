module.exports = {
  description: "Replies with a message checking Galleom's latency.",
  aliases: ['pong']
};

module.exports.action = (_client, msg) => {
  msg.channel.send('Pong!').then((msg2) => {
    msg2.edit(`Pong! \`${msg2.createdTimestamp - msg.createdTimestamp}ms\``);
  });
};
