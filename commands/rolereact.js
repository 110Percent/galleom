const db = require('../core/database');
const roleModule = require('../core/rolereact');

module.exports = {
  usage: '<channel id> <message id> <emoji> <role id> <name>',
  description: 'Maps a reaction on a message to a role on the server.',
  example: '=rr 012345 67890 👌 789012 okbro',
  aliases: ['rr'],
  hidden: true,
  modOnly: true
};

module.exports.action = (client, msg, args) => {
  if (!args) {
    return;
  }
  const opts = {
    role: args.split(' ')[3].replace(/\D/g, ''),
    channel: args.split(' ')[0].replace(/\D/g, ''),
    message: args.split(' ')[1].replace(/\D/g, ''),
    reaction: args.split(' ')[2],
    name: args.split(' ')[4]
  };
  if (/\d/g.test(opts.reaction)) {
    opts.reaction = opts.reaction.replace(/\D/g, '');
  }
  db.reactionRoles.create(opts).then(() => {
    if (client.emojis.get(opts.reaction) || /\D/g.test(opts.reaction)) {
      client.channels
        .get(opts.channel)
        .fetchMessage(opts.message)
        .then((m) => {
          m.react(opts.reaction).then(() => {
            msg.channel.send('Reaction role set!');
            roleModule.refresh();
          });
        });
    } else {
      msg.channel.send('Reaction role set!');
      roleModule.refresh();
    }
  });
};
