const db = require('../core/database');
const config = require('../config/config');

module.exports = {
  usage: '<name>',
  description: 'Fetches a tag from the database.',
  example: '=tag',
  aliases: ['t']
};

module.exports.action = (client, msg, args) => {
  if (!args) return;

  if (args.split(' ')[0] === 'add') {
    if (
      client.guilds
        .get(config.mainServer)
        .members.get(msg.author.id)
        .roles.get(config.modRole)
      || client.guilds
        .get(config.mainServer)
        .members.get(msg.author.id)
        .roles.get(config.trialModRole)
    ) {
      const content = args.split(' ');
      content.splice(0, 2);
      db.tags.findOne({ where: { name: args.split(' ')[1].toLowerCase() } }).then((v) => {
        if (v === null) {
          db.tags
            .create({
              name: args.split(' ')[1].toLowerCase(),
              value: content.join(' ')
            })
            .then(() => {
              msg.channel.send(`📝 Created tag \`${args.split(' ')[1].toLowerCase()}\`!`);
            });
        } else {
          msg.channel.send(`❌ Tag \`${args.split(' ')[1].toLowerCase()}\` already exists!`);
        }
      });
    }
  } else {
    db.tags.findOne({ where: { name: args.split(' ')[0].toLowerCase() } }).then((v) => {
      if (v === null) {
        msg.channel.send("Couldn't find a tag with that name...");
      } else {
        console.log(v);
        msg.channel.send(v.dataValues.value);
      }
    });
  }
};
