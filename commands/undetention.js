const detention = require('./detention');
const db = require('../core/database');
const config = require('../config/config');

module.exports = {
  usage: '<user ID>',
  description: 'Removes a user from detention and saves the channel messages to a log.',
  example: `=undetention ${config.ownerID}`,
  aliases: ['undet'],
  hidden: true,
  modOnly: true
};

module.exports.action = async (client, msg, args) => {
  const query = args.split(' ')[0];
  if (!new RegExp('^\\d+$').test(query)) {
    msg.channel.send('User ID not provided.');
    return;
  }

  const member = client.guilds.get(config.mainServer).members.get(query);
  let user;
  if (member) {
    ({ user } = member);
  }
  let cUser = await db.cachedUsers.findOne({ where: { userID: query } });
  if (cUser) {
    cUser = cUser.dataValues;
    cUser.seqID = cUser.id;
    cUser.id = cUser.userID;
  }
  if (!user && !cUser) {
    msg.channel.send('❌ User not found.');
    return;
  }
  msg.channel
    .send(`Are you sure you want to remove User \`${(user || cUser).username}\` from detention?`)
    .then((msg2) => {
      msg2.react('👌');
      msg2
        .awaitReactions((r, u) => r.emoji.name === '👌' && u.id === msg.author.id, { max: 1 })
        .then((collected) => {
          if (collected.size > 0) {
            detention.undetention((user || cUser).id, client, msg);
          }
        })
        .catch(console.error);
    });
};
