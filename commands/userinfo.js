const Discord = require('discord.js');
const moment = require('moment');
const config = require('../config/config');

module.exports = {
  usage: '[user]',
  description:
    "Gets information on a user. If no user is specified, the sender's information is displayed.",
  example: `=userinfo <@${config.ownerID}>`
};

module.exports.action = async (client, msg, args) => {
  let query = args.split(' ')[0];
  query = query.match(/\d+/) ? query.match(/\d+/)[0] : msg.author.id;

  let user = await client.fetchUser(query);
  if (!user) {
    user = msg.guild.members.get(query)
      ? msg.guild.members.get(query).user
      : client.users.get(query);
  }
  const embed = new Discord.RichEmbed();

  if (!user || typeof user === 'undefined') {
    msg.channel.send('User not found. Please use a valid mention or ID.');
    return;
  }

  embed
    .setAuthor(user.tag, user.displayAvatarURL)
    .setThumbnail(user.displayAvatarURL)
    .addField('ID', user.id, true);

  let member;
  if (client.guilds.get(config.mainServer).members.get(query)) {
    member = client.guilds.get(config.mainServer).members.get(query);
  } else if (msg.guild) {
    member = msg.guild.members.get(query);
  }

  if (member) {
    if (member.nickname) {
      embed.addField('Nickname', member.nickname, true);
    }
    const roles = member.roles.map(r => r.name);
    embed.addField('Roles', roles.join(', '));
  }

  const created = moment(user.createdTimestamp).calendar();
  embed.addField('Account Created', created, true);

  if (member) {
    const joined = moment(member.joinedAt).calendar();
    embed.addField('Joined Server', joined, true);
  }

  msg.channel.send(embed);
};
