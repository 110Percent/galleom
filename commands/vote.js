const letters = [
  '🇦',
  '🇧',
  '🇨',
  '🇩',
  '🇪',
  '🇫',
  '🇬',
  '🇭',
  '🇮',
  '🇯',
  '🇰',
  '🇱',
  '🇲',
  '🇳',
  '🇴',
  '🇵',
  '🇶',
  '🇷',
  '🇸',
  '🇹',
  '🇺',
  '🇻',
  '🇼',
  '🇽',
  '🇾',
  '🇿'
];

module.exports = {
  usage: '[range] [letter/number]',
  description:
    'Creates a vote on the previous message. Add `range <letter/number>` after the command to start a range vote leading up to the entered letter/number. Adding nothing will start a yes/no vote.',
  example: '=vote\n=vote range 4\n=vote range c'
};

async function fillNums(m, max) {
  for (let i = 1; i < max + 1; i++) {
    if (i === 10) {
      await m.react(`${0}⃣`);
      break;
    }
    if (i > max) break;

    await m.react(`${i}⃣`);
  }
}

async function fillLetters(m, max) {
  const maxID = max.charCodeAt(0) - 97;
  for (let i = 0; i < maxID + 1; i++) {
    if (i > max) break;

    await m.react(letters[i]);
  }
}

module.exports.action = (client, msg, args) => {
  msg.delete().then((deleted) => {
    const ch = deleted.channel;
    ch.fetchMessages({ limit: 1 }).then((msgs) => {
      const m = msgs.array()[0];
      if (args) {
        if (args.split(' ')[0] === 'range') {
          const max = args
            .split(' ')[1]
            .substring(0, 1)
            .toLowerCase();
          if ('0123456789'.includes(max)) {
            fillNums(m, max);
          } else if (max.match(/[a-z]/i)) {
            fillLetters(m, max);
          }
        }
      } else {
        m.react(client.emojis.get('556164445794074657')).then(() => {
          m.react(client.emojis.get('556164445491953679'));
        });
      }
    });
  });
};
