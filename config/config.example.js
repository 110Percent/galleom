module.exports = {
  token: '',
  prefix: '',
  ownerID: '',
  mainServer: '',
  modServer: '',
  logChannel: '',
  modRole: '',
  trialModRole: '',
  flagChannel: '',
  database: {
    host: '',
    port: 8888,
    user: '',
    password: ''
  }
};
