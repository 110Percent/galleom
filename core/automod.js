/* eslint-disable no-loop-func */
const noAccent = require('remove-accents');
const Discord = require('discord.js');
const db = require('./database');
const config = require('../config/config');

let flagChannels;

module.exports.init = (channels) => {
  flagChannels = channels;
};

module.exports.scan = (msg) => {
  const check = noAccent(msg.content.toLowerCase()).replace(/[^a-z ]/gi, '');
  if (
    msg.guild.id !== config.mainServer
    || msg.member.roles.get(config.modRole)
    || msg.member.roles.get(config.trialModRole)
    || msg.author.bot
  ) {
    return;
  }
  db.bannedWords.findAll().then((words) => {
    for (let i = 0; i < words.length; i++) {
      const regex = new RegExp(words[i].dataValues.regex, 'gim');
      if (regex.test(check)) {
        msg.delete().then((delMsg) => {
          const warnEmbed = new Discord.RichEmbed();
          warnEmbed.setTitle('Automod Report');
          let reportString = `Your message was deleted from <#${
            delMsg.channel.id
          }> for containing an instance of the following word: **${words[i].dataValues.name}**.\n`;
          reportString
            += 'Your message has been sent to the moderation team for review, however no other automatic actions have taken place.';
          reportString += '\n\nIf you have any questions or concerns, please contact a moderator.';
          warnEmbed.setDescription(reportString);
          delMsg.author.send(warnEmbed);

          const embed = new Discord.RichEmbed();
          embed
            .setColor('#ED553B')
            .setAuthor(`${delMsg.author.tag} (${delMsg.author.id})`, delMsg.author.displayAvatarURL)
            .setTitle('⚠ Automod Flag Report')
            .addField('Message ID', delMsg.id, true)
            .addField('Channel', delMsg.channel, true)
            .addField('Content', delMsg.content || '<IMAGE>')
            .setTimestamp();
          flagChannels.forEach(c => c.send(embed));
        });
        break;
      }
    }
  });
};
