const Sequelize = require('sequelize');
const config = require('../config/config');

const sequelize = new Sequelize('galleom', config.database.user, config.database.password, {
  dialect: 'postgres',
  host: config.database.host,
  port: config.database.port,
  logging: false
});

const BannedWord = sequelize.define('bannedWord', {
  name: { type: Sequelize.STRING },
  regex: { type: Sequelize.STRING }
});

const Modnote = sequelize.define('modnote', {
  user: { type: Sequelize.STRING(20) },
  index: { type: Sequelize.INTEGER },
  content: { type: Sequelize.STRING(1024) },
  author: { type: Sequelize.STRING(20) }
});

const Offence = sequelize.define('offence', {
  user: { type: Sequelize.STRING(20) },
  index: { type: Sequelize.INTEGER },
  type: { type: Sequelize.STRING(32) }
});

const FriendCode = sequelize.define('friendCode', {
  user: { type: Sequelize.STRING(20) },
  code: { type: Sequelize.STRING(32) },
  console: { type: Sequelize.INTEGER }
});

const SmashConsole = sequelize.define('console', {
  index: { type: Sequelize.INTEGER },
  name: { type: Sequelize.STRING(32) },
  regex: { type: Sequelize.STRING(64) },
  regPlaceholder: { type: Sequelize.STRING(32) }
});

const MuteCache = sequelize.define('muteCache', {
  user: { type: Sequelize.STRING(20) },
  roles: { type: Sequelize.TEXT },
  channel: { type: Sequelize.STRING(20) }
});

const CachedRoles = sequelize.define('cachedRole', {
  roleID: { type: Sequelize.STRING(30) }
});

const ChannelLogs = sequelize.define('channelLog', {
  channel: { type: Sequelize.STRING(20) },
  channelName: { type: Sequelize.STRING(64) },
  logs: { type: Sequelize.TEXT }
});

const ReactionRoles = sequelize.define('reactionRole', {
  role: { type: Sequelize.STRING(32) },
  channel: { type: Sequelize.STRING(32) },
  message: { type: Sequelize.STRING(32) },
  reaction: { type: Sequelize.STRING(32) },
  name: { type: Sequelize.STRING(64) }
});

const Tags = sequelize.define('tag', {
  name: { type: Sequelize.STRING(32) },
  value: { type: Sequelize.TEXT }
});

const CachedUsers = sequelize.define('cachedUser', {
  userID: { type: Sequelize.STRING(32) },
  username: { type: Sequelize.STRING(32) },
  discriminator: { type: Sequelize.STRING(4) },
  displayAvatarURL: { type: Sequelize.STRING(128) }
});

const Levels = sequelize.define('level', {
  user: { type: Sequelize.STRING(32) },
  exp: { type: Sequelize.INTEGER },
  level: { type: Sequelize.INTEGER }
});

sequelize
  .authenticate()
  .then(() => {
    console.log('Database connection established successfully.');

    BannedWord.sync({ force: false });
    Modnote.sync({ force: false });
    Offence.sync({ force: false });
    FriendCode.sync({ force: false });
    SmashConsole.sync({ force: false });
    MuteCache.sync({ force: false });
    CachedRoles.sync({ force: false });
    ChannelLogs.sync({ force: false });
    ReactionRoles.sync({ force: false });
    Tags.sync({ force: false });
    CachedUsers.sync({ force: false });
    Levels.sync({ force: false });
  })
  .catch((err) => {
    console.error('Unable to connect to the database:', err);
  });

module.exports.bannedWords = BannedWord;
module.exports.modnotes = Modnote;
module.exports.offences = Offence;
module.exports.friendCodes = FriendCode;
module.exports.consoles = SmashConsole;
module.exports.muteCache = MuteCache;
module.exports.cachedRoles = CachedRoles;
module.exports.channelLogs = ChannelLogs;
module.exports.reactionRoles = ReactionRoles;
module.exports.tags = Tags;
module.exports.cachedUsers = CachedUsers;
module.exports.levels = Levels;
