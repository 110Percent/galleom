const db = require('./database');

const msgTimestamps = {};

module.exports.init = async (client) => {
  client.on('message', async (msg) => {
    if (
      !msgTimestamps[msg.author.id]
      || msgTimestamps[msg.author.id] + 60000 < msg.createdTimestamp
    ) {
      msgTimestamps[msg.author.id] = msg.createdTimestamp;
      let levelData = await db.levels.findOne({ where: { user: msg.author.id } });
      if (levelData) {
        levelData = levelData.dataValues;
      } else {
        levelData = {
          user: msg.author.id,
          exp: 0,
          level: 1
        };
      }
      levelData.exp++;
      let power = 1;
      let index = 0;
      while (levelData.exp > power * 100) {
        index++;
        power *= 2;
      }
      levelData.level = index + 1;
      db.levels.upsert(levelData, { where: { user: msg.author.id } });
    }
  });
};
