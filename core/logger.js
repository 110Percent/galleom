const Discord = require('discord.js');
const moment = require('moment');
const config = require('../config/config');

let client;
const logChannels = [];

module.exports.init = (c) => {
  client = c;
  for (let i = 0; i < config.logChannels.length; i++) {
    logChannels.push(client.channels.get(config.logChannels[i]));
  }
};

const doLog = (embed) => {
  for (let i = 0; i < logChannels.length; i++) {
    logChannels[i].send(embed);
  }
};

module.exports.logDelete = (msg) => {
  const embed = new Discord.RichEmbed();
  embed
    .setColor('#ED553B')
    .setAuthor(msg.author.tag, msg.author.displayAvatarURL)
    .setTitle('🗑 Message Deleted')
    .addField('Message ID', msg.id, true)
    .addField('Channel', msg.channel, true)
    .addField('Content', msg.content || '<IMAGE>')
    .setTimestamp();
  doLog(embed);
};

module.exports.logEdit = (msg, msg2) => {
  const embed = new Discord.RichEmbed();
  embed
    .setColor('#F2B134')
    .setAuthor(msg.author.tag, msg.author.displayAvatarURL)
    .setTitle('📝 Message Edited')
    .addField('Message ID', msg.id, true)
    .addField('Channel', msg.channel, true)
    .addField('Original Content', msg.content || '<empty>')
    .addField('Edited Content', msg2.content || '<empty>')
    .setTimestamp();
  doLog(embed);
};

module.exports.logLeave = (member) => {
  const embed = new Discord.RichEmbed();
  embed
    .setColor('#112F41')
    .setAuthor(member.user.tag, member.user.displayAvatarURL)
    .setTitle('👋 User Left')
    .addField('User ID', member.user.id, true)
    .addField('Roles', member.roles.map(r => r.name).join(', '))
    .setTimestamp();
  doLog(embed);
};

module.exports.logJoin = (member) => {
  const embed = new Discord.RichEmbed();
  embed
    .setColor('#0894A1')
    .setAuthor(member.user.tag, member.user.displayAvatarURL)
    .setTitle('👤 User Joined')
    .addField('User ID', member.user.id, true)
    .addField('Account Created', moment(member.user.createdTimestamp).calendar())
    .setTimestamp();
  doLog(embed);
};

module.exports.logVoiceJoin = (member) => {
  const embed = new Discord.RichEmbed();
  embed
    .setColor('#785AAB')
    .setAuthor(member.user.tag, member.user.displayAvatarURL)
    .setTitle('🎙 User Joined Voice')
    .addField('User ID', member.user.id, true)
    .addField('Voice Channel', member.voiceChannel.name, true)
    .setTimestamp();
  doLog(embed);
};

module.exports.logVoiceLeave = (member) => {
  const embed = new Discord.RichEmbed();
  embed
    .setColor('#4F3B70')
    .setAuthor(member.user.tag, member.user.displayAvatarURL)
    .setTitle('🚪 User Left Voice')
    .addField('User ID', member.user.id, true)
    .addField('Voice Channel', member.voiceChannel.name, true)
    .setTimestamp();
  doLog(embed);
};
