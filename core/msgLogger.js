let client;
const logChannels = [];
const msgLogs = {};

module.exports.init = (c) => {
  client = c;
  client.on('message', (msg) => {
    if (logChannels.indexOf(msg.channel.id) < 0) return;

    if (!msgLogs[msg.channel.id]) {
      msgLogs[msg.channel.id] = {};
    }

    msgLogs[msg.channel.id][msg.id] = {
      author: msg.author.id,
      authorTag: msg.author.tag,
      content: msg.cleanContent,
      timestamp: msg.createdTimestamp
    };
  });
};

module.exports.startLogging = (channel) => {
  if (logChannels.indexOf(channel) < 0) {
    logChannels.push(channel);
  }
};

module.exports.stopLogging = (channel) => {
  if (logChannels.indexOf(channel) > -1) {
    logChannels.splice(logChannels.indexOf(channel), 1);
  }
};

module.exports.logs = msgLogs;
