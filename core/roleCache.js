const db = require('./database');
const config = require('../config/config');

let client;
let mainServer;

exports.init = async (c) => {
  client = c;
  mainServer = client.guilds.get(config.mainServer);
  await db.cachedRoles.destroy({
    where: {},
    truncate: true
  });
  mainServer.roles.forEach((r) => {
    db.cachedRoles.findOrCreate({ where: { roleID: r.id } });
  });
};
