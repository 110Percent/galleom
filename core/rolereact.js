const db = require('./database');

let cache = [];

const refresh = (client) => {
  db.reactionRoles.findAll().then((links) => {
    cache = links.map(c => c.dataValues);
    if (client) {
      cache.forEach((r) => {
        client.channels
          .get(r.channel)
          .fetchMessage(r.message)
          .catch(() => {
            // Silent
          });
      });
    }
  });
};

module.exports.init = (client) => {
  refresh(client);
  client.on('messageReactionAdd', (reaction, user) => {
    const id = reaction.emoji.id || reaction.emoji.name;
    for (let i = 0; i < cache.length; i++) {
      if (
        cache[i].channel === reaction.message.channel.id
        && cache[i].message === reaction.message.id
        && id === cache[i].reaction
      ) {
        const member = reaction.message.guild.members.get(user.id);
        member.addRole(cache[i].role);
      }
    }
  });
  client.on('messageReactionRemove', (reaction, user) => {
    const id = reaction.emoji.id || reaction.emoji.name;
    for (let i = 0; i < cache.length; i++) {
      if (
        cache[i].channel === reaction.message.channel.id
        && cache[i].message === reaction.message.id
        && id === cache[i].reaction
      ) {
        const member = reaction.message.guild.members.get(user.id);
        member.removeRole(cache[i].role);
      }
    }
  });
};

module.exports.refresh = refresh;
