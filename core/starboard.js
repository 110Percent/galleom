const Discord = require('discord.js');
const config = require('../config/config');

const boardEntries = {};
const stars = {
  5: '⭐',
  8: '🌟',
  11: '🌠'
};
const imageExtensions = ['png', 'jpg', 'jpeg', 'gif'];

module.exports.init = async (client) => {
  const starChannel = client.channels.get(config.starChannel);

  client.on('messageReactionAdd', (reaction, user) => {
    if (reaction.emoji.name === '⭐') {
      if (user.id !== reaction.message.author.id) {
        if (boardEntries[reaction.message.id]) {
          boardEntries[reaction.message.id].users.push(user.id);
        } else {
          boardEntries[reaction.message.id] = {
            users: [user.id],
            postID: null
          };
        }
        if (reaction.count < Object.keys(stars)[0]) {
          return;
        }
        const msg = reaction.message;
        let star;
        for (let i = Object.values(stars).length; i > -1; i--) {
          if (reaction.count >= Object.keys(stars)[i]) {
            star = Object.values(stars)[i];
            break;
          }
        }
        const content = `${star} **${reaction.count}**`;
        const embed = new Discord.RichEmbed({
          author: {
            name: msg.member.displayName,
            icon_url: msg.author.displayAvatarURL
          },
          description: msg.content,
          fields: [
            {
              name: 'Original',
              value: `[Jump!](${msg.url})`
            }
          ]
        });
        if (msg.attachments.size > 0) {
          const first = msg.attachments.array()[0];
          const ext = first.filename.split('.')[first.filename.split('.').length - 1];
          if (imageExtensions.indexOf(ext) > -1) {
            embed.setImage(first.url);
          } else {
            embed.attachFile({ attachment: first.url, name: first.filename });
          }
        }
        // eslint-disable-next-line eqeqeq
        if (reaction.count == Object.keys(stars)[0]) {
          starChannel.send(content, embed).then((m) => {
            boardEntries[reaction.message.id].postID = m.id;
          });
        } else if (reaction.count > Object.keys(stars)[0]) {
          starChannel.fetchMessage(boardEntries[reaction.message.id].postID).then((m) => {
            m.edit(content, embed);
          });
        }
      } else {
        reaction.message.react('🤡');
      }
    }
  });

  client.on('messageReactionRemove', async (reaction, _user) => {
    if (reaction.count < Object.keys(stars)[0] && boardEntries[reaction.message.id]) {
      const post = await starChannel.fetchMessage(boardEntries[reaction.message.id].postID);
      if (post) {
        post.delete();
      }
    }
  });
};
