const express = require('express');
const moment = require('moment');
const fs = require('fs');
const path = require('path');
const Autolinker = require('autolinker');
const config = require('../config/config');

const app = express();
const db = require('./database');

const autolinker = new Autolinker({ stripPrefix: { scheme: false } });
let client;

app.use(express.static('core/web'));

module.exports.init = (c) => {
  client = c;
};

app.get('/logs/:channelID', async (req, res) => {
  if (Number.isNaN(req.params.channelID)) {
    res.send();
    return;
  }
  db.channelLogs.findOne({ where: { channel: req.params.channelID } }).then((entry) => {
    const logBase = fs.readFileSync(path.join(__dirname, 'logBase.html'), 'utf8');
    const logEntry = fs.readFileSync(path.join(__dirname, 'logEntry.html'), 'utf8');
    if (!entry) {
      res.send('');
      return;
    }
    let toSend = logBase;
    const logs = JSON.parse(entry.dataValues.logs);
    toSend = toSend.replace('{channelname}', entry.dataValues.channelName);
    const logElements = [];
    for (let i = 0; i < Object.values(logs).length; i++) {
      const log = Object.values(logs)[i];
      let lElement = JSON.parse(JSON.stringify(logEntry));
      const author = client.users.get(log.author);
      let cAuthor = db.cachedUsers.findOne({ where: { userID: log.author } });
      if (cAuthor && !author) {
        cAuthor = cAuthor.dataValues;
        cAuthor.seqID = cAuthor.id;
        cAuthor.id = cAuthor.userID;
      }
      lElement = lElement.replace('{avatar}', (author || cAuthor).displayAvatarURL);
      lElement = lElement.replace('{uname}', (author || cAuthor).username);
      lElement = lElement.replace('{timestamp}', moment(log.timestamp).calendar());
      lElement = lElement.replace('{content}', autolinker.link(log.content));
      logElements.push(lElement);
    }
    toSend = toSend.replace('{logs}', logElements.join('\n'));
    res.send(toSend);
  });
});

app.listen(config.webserver.port, () => console.log(`Example app listening on port ${config.webserver.port}!`));
