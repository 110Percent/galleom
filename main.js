const Discord = require('discord.js');
const fs = require('fs');
const path = require('path');
const config = require('./config/config');
const logger = require('./core/logger');
const automod = require('./core/automod');
const roleCache = require('./core/roleCache');
const msgLogger = require('./core/msgLogger');
const webServer = require('./core/webserver');
const roleReact = require('./core/rolereact');
const starboard = require('./core/starboard');
const levelling = require('./core/levelling');

const galleom = new Discord.Client({ fetchAllMembers: true });

// Load commands from the commands directory
const commands = {};
const aliases = { _all: [] };
const fileList = fs.readdirSync(`${path.join(__dirname, './commands')}`);
for (let i = 0; i < fileList.length; i++) {
  const cmdFile = fileList[i];
  commands[cmdFile.split('.')[0]] = require(`./commands/${cmdFile}`);
  [commands[cmdFile.split('.')[0]].name] = cmdFile.split('.');
  aliases[cmdFile.split('.')[0]] = commands[cmdFile.split('.')[0]].aliases || [];
  aliases._all = aliases._all.concat(aliases[cmdFile.split('.')[0]]);
}

// Message handler
galleom.on('message', (msg) => {
  // Scan non-bot and non-mod messages with Galleom's automod
  const mainMember = galleom.guilds.get(config.mainServer).members.get(msg.author.id);
  automod.scan(msg);

  // Command handler
  if (msg.content.startsWith(config.prefix)) {
    // Get attempted command name
    let attCmd = msg.content.split(' ')[0].substr(config.prefix.length);

    // Check for an alias and handle it
    if (aliases._all.indexOf(attCmd) > -1) {
      attCmd = Object.keys(aliases).find(
        key => aliases[key].indexOf(attCmd) > -1 && key !== '_all'
      );
    }

    // Handle command
    if (commands[attCmd]) {
      // Filter out unauthorized attempts of mod-only and owner-only commands
      if (
        commands[attCmd].modOnly
        && !mainMember.roles.get(config.modRole)
        && !mainMember.roles.get(config.trialModRole)
      ) {
        return;
      }
      if (commands[attCmd].ownerOnly && msg.author.id !== config.ownerID) {
        return;
      }

      let args = msg.content.split(' ');
      args.splice(0, 1);
      args = args.join(' ');
      commands[attCmd].action(galleom, msg, args, commands, aliases);
    }
  }
});

// Handle events for logging
// TODO: Cleanup

galleom.on('messageDelete', (msg) => {
  if (msg.guild && msg.guild.id === config.mainServer) {
    logger.logDelete(msg);
  }
});

galleom.on('messageUpdate', (msg, msg2) => {
  if (msg.guild && msg.guild.id === config.mainServer && msg.content !== msg2.content) {
    logger.logEdit(msg, msg2);
  }
});

galleom.on('guildMemberRemove', (member) => {
  if (member.guild.id === config.mainServer) {
    logger.logLeave(member);
  }
});

galleom.on('guildMemberAdd', (member) => {
  if (member.guild.id === config.mainServer) {
    logger.logJoin(member);
  }
});

galleom.on('voiceStateUpdate', (member, member2) => {
  if (member.guild.id === config.mainServer) {
    if (member.voiceChannel) {
      logger.logVoiceLeave(member);
    }
    if (member2.voiceChannel) {
      logger.logVoiceJoin(member2);
    }
  }
});

// Initialize core modules on startup
galleom.on('ready', () => {
  console.log(`Logged in as ${galleom.user.tag}`);
  logger.init(galleom);
  automod.init(config.flagChannels.map(m => galleom.channels.get(m)));
  roleCache.init(galleom);
  msgLogger.init(galleom);
  webServer.init(galleom);
  roleReact.init(galleom);
  starboard.init(galleom);
  levelling.init(galleom);
});

galleom.login(config.token);
